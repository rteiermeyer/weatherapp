package mobilprog.weatherapp;

import java.io.Serializable; // to be able to pass it to another activity

public class WeatherData implements Serializable {
    private String location;
    private String weather_type;
    private double current_temperature;
    private double min_temperature;
    private double max_temperature;
    private String timestamp;

    // constructor
    public WeatherData(String location,
                       String weather_type,
                       double current_temperature,
                       double min_temperature,
                       double max_temperature,
                       String timestamp){
        this.location = location;
        this.weather_type = weather_type;
        this.current_temperature = current_temperature;
        this.min_temperature = min_temperature;
        this.max_temperature = max_temperature;
        this.timestamp = timestamp;
    }

    // getters
    public String getLocation(){
        return location;
    }

    public String getWeatherType(){
        return weather_type;
    }

    public double getCurrentTemperature(){
        return current_temperature;
    }

    public double getMinTemperature() {
        return min_temperature;
    }

    public double getMaxTemperature() {
        return max_temperature;
    }

    public String getTimestamp() {
        return timestamp;
    }
}
