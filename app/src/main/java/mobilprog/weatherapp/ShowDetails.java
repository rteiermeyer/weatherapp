package mobilprog.weatherapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ShowDetails extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        // get extra from calling activity
        ArrayList<WeatherData> daily_data = (ArrayList<WeatherData>) getIntent().getSerializableExtra("daily_data");

        // set date
        SimpleDateFormat date_format = new SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH);

        TextView details_date = findViewById(R.id.details_date);
        details_date.setText(String.valueOf(date_format.format(new Date(Long.valueOf(daily_data.get(0).getTimestamp()) * 1000))));

        // load detailed daily data
        loadDetails(daily_data);
    }

    void loadDetails(ArrayList<WeatherData> daily_data){
        RecyclerView details_recycler = findViewById(R.id.detailsrecycler);

        details_recycler.setHasFixedSize(true); // improves performance

        // initialize the recyclerview adapter
        details_recycler.setAdapter(new DetailsRecyclerViewAdapter(this, daily_data));

        // create a linear layout manager for the recyclerview
        details_recycler.setLayoutManager(new LinearLayoutManager(this));
    }
}
