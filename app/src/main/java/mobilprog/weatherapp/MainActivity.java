package mobilprog.weatherapp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    static final int FINE_LOCATION_PERMISSION = 0;
    static final int COARSE_LOCATION_PERMISSION = 1;

    LocationManager locationManager;
    LastKnownLocation lastKnownLocation;

    private requestCurrentWeatherTask requestCurrentWeatherTask;
    private requestWeeklyForecastTask requestWeeklyForecastTask;

    WeatherData current_data;
    ArrayList<ArrayList<WeatherData>> weekly_data;

    ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // set onclick listener to refresh button
        findViewById(R.id.refresh_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestWeatherData();
            }
        });

        final Switch fahrenheit_switch = findViewById(R.id.fahrenheit_switch);
        fahrenheit_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                requestWeatherData();
            }
        });

        // register shake detector
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
        mAccel = 0.00f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;

        // start spinner
        spinner = (ProgressBar)findViewById(R.id.spinner);
        spinner.setVisibility(View.VISIBLE);

        // register a location listener & try to request data
        registerLocationListener();
        requestWeatherData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (requestWeeklyForecastTask != null) {
            requestWeeklyForecastTask.cancel(true);
        }
        if (requestCurrentWeatherTask != null) {
            requestCurrentWeatherTask.cancel(true);
        }
    }

    private static final float SHAKE_THRESHOLD = 3.25f; // m/S**2
    private static final int MIN_TIME_BETWEEN_SHAKES_MILLISECS = 1000;
    private SensorManager mSensorManager;
    private float mAccel; // acceleration apart from gravity
    private float mAccelCurrent; // current acceleration including gravity
    private float mAccelLast; // last acceleration including gravity
    private long mLastShakeTime;

    private final SensorEventListener mSensorListener = new SensorEventListener() {

        public void onSensorChanged(SensorEvent se) {
            if (se.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                long curTime = System.currentTimeMillis();
                if ((curTime - mLastShakeTime) > MIN_TIME_BETWEEN_SHAKES_MILLISECS) {
                    float x = se.values[0];
                    float y = se.values[1];
                    float z = se.values[2];
                    mAccelLast = mAccelCurrent;
                    mAccelCurrent = (float) Math.sqrt((double) (x * x + y * y + z * z));
                    float delta = mAccelCurrent - mAccelLast;
                    mAccel = mAccel * 0.9f + delta;

                    if (mAccel > SHAKE_THRESHOLD) {
                        Log.d("Debug", "SHAKE DETECTED");

                        mLastShakeTime = curTime;

                        // if shake is detected, request for weather data
                        requestWeatherData();

                    }
                    // Log.d("Debug", "Acceleration: " + mAccel);
                }
            }
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(mSensorListener);
        super.onPause();
    }

    class LastKnownLocation {
        double lat, lon;

        public LastKnownLocation(double lat, double lon) {
            this.lat = lat;
            this.lon = lon;
        }

        public double getLat() {
            return lat;
        }

        public double getLon() {
            return lon;
        }
    }

    void registerLocationListener() {
        Log.d("Debug","Creating new location listener.");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("Debug", "Permission requested by location listener");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, FINE_LOCATION_PERMISSION);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, COARSE_LOCATION_PERMISSION);
            return;
        }

        LocationListener locationListener = new LocationListener() {

            public void onLocationChanged(Location location) {
                double lat = location.getLatitude();
                double lon = location.getLongitude();

                lastKnownLocation = new LastKnownLocation(lat, lon);

                Log.d("VARIABLES", lat + ", " + lon);
            }

            public void onProviderEnabled(String provider) {
                Log.d("Debug","Provider enabled");
            }

            public void onProviderDisabled(String provider) {
                Log.d("Debug","Provider disabled");
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.d("Debug","Provider status changed");
            }
        };

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("Debug","No permissions");
            return;
        }

        // get location updates every 10 seconds
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, locationListener);
    }

    private static class taskParameters {
        double latitude, longitude;
        boolean fahrenheit;

        taskParameters(double latitude, double longitude, boolean fahrenheit) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.fahrenheit = fahrenheit;
        }

        double getLatitude() {
            return latitude;
        }

        double getLongitude() {
            return longitude;
        }

        boolean isFahrenheit() {
            return fahrenheit;
        }
    }

    private class requestCurrentWeatherTask extends AsyncTask<taskParameters, Void, Void> {
        @Override
        protected Void doInBackground(taskParameters... taskParameters) {
            double latitude = taskParameters[0].getLatitude();
            double longitude = taskParameters[0].getLongitude();
            boolean fahrenheit = taskParameters[0].isFahrenheit();

            requestCurrentWeather(latitude, longitude, fahrenheit);
            return null;
        }
    }

    private class requestWeeklyForecastTask extends AsyncTask<taskParameters, Void, Void> {
        @Override
        protected Void doInBackground(taskParameters... taskParameters) {
            double latitude = taskParameters[0].getLatitude();
            double longitude = taskParameters[0].getLongitude();
            boolean fahrenheit = taskParameters[0].isFahrenheit();

            requestWeeklyForecast(latitude, longitude, fahrenheit);
            return null;
        }
    }

    void requestWeatherData() {

        // show spinner
        spinner.setVisibility(View.VISIBLE);

        // permission check
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("Debug", "Permission requested by refresher");
            // ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
            // ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            // edit: no need to request permissions here
            return;
        }

//        final double latitude = 47.0714222;
//        final double longitude = 17.6093827;

        final double latitude;
        final double longitude;
        try {
            latitude = lastKnownLocation.getLat();
            longitude = lastKnownLocation.getLon();
        }
        catch(NullPointerException ex){
            Log.d("Debug","GPS information not available");
            return;
        }

        final boolean fahrenheit;

        Switch fahrenheit_switch = findViewById(R.id.fahrenheit_switch);
        if (fahrenheit_switch.isChecked()) {
            fahrenheit = true;
        } else {
            fahrenheit = false;
        }

        if (requestCurrentWeatherTask != null) {
            requestCurrentWeatherTask.cancel(true);
        }
        if (requestWeeklyForecastTask != null) {
            requestWeeklyForecastTask.cancel(true);
        }

        requestCurrentWeatherTask = new requestCurrentWeatherTask();
        requestWeeklyForecastTask = new requestWeeklyForecastTask();
        requestCurrentWeatherTask.execute(new taskParameters(latitude, longitude, fahrenheit));
        requestWeeklyForecastTask.execute(new taskParameters(latitude, longitude, fahrenheit));
    }

    void requestCurrentWeather(double latitude, double longitude, boolean fahrenheit) {
        // unit types:
        // fahrenheit: &units=imperial
        // celsius: &units=metric

        // example link using celsius as unit:
        // http://api.openweathermap.org/data/2.5/weather?lat=47.1257777&lon=17.8372092&appid=8d5bc773ee5432df7c786caba661b714&units=metric

        String url = "http://api.openweathermap.org/data/2.5/weather?" +
                "lat=" + String.valueOf(latitude) +
                "&lon=" + String.valueOf(longitude) +
                "&appid=8d5bc773ee5432df7c786caba661b714";

        // set unit to request
        if (fahrenheit) {
            url += "&units=imperial";
        } else {
            url += "&units=metric";
        }

        // request queue
        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        String location_name = "LOCATION_NAME"; // -> string
                        String weather_type = "WEATHER_TYPE"; // -> string
                        String current_temperature = "0.0"; // -> double
                        String min_temperature = "0.0"; // -> double
                        String max_temperature = "0.0"; // -> double
                        String timestamp = "TIMESTAMP"; // -> string

                        try {
                            // get current location name
                            location_name = response.getString("name");
                        } catch (JSONException e) {
                            Toast.makeText(MainActivity.this, "Requested data could not be parsed!", Toast.LENGTH_SHORT).show();
                            Log.d("DEBUG", "Location name could not be determined!");
                        }

                        try {
                            // get weather type
                            JSONArray weather = response.getJSONArray("weather");

                            for (int i = 0; i < weather.length(); i++) {
                                JSONObject tmp_obj = weather.getJSONObject(i);
                                weather_type = tmp_obj.getString("main");
                            }
                        } catch (JSONException e) {
                            Toast.makeText(MainActivity.this, "Requested data could not be parsed!", Toast.LENGTH_SHORT).show();
                            Log.d("DEBUG", "Weather type could not be determined!");
                        }

                        try {
                            // get temperatures
                            JSONObject main = response.getJSONObject("main");

                            current_temperature = main.getString("temp");
                            min_temperature = main.getString("temp_min");
                            max_temperature = main.getString("temp_max");

                        } catch (JSONException e) {
                            Toast.makeText(MainActivity.this, "Requested data could not be parsed!", Toast.LENGTH_SHORT).show();
                            Log.d("DEBUG", "Temperatures could not be determined!");
                        }

                        try {
                            // get current date
                            timestamp = response.getString("dt");
                        } catch (JSONException e) {
                            Toast.makeText(MainActivity.this, "Requested data could not be parsed!", Toast.LENGTH_SHORT).show();
                            Log.d("DEBUG", "Current date could not be determined!");
                        }

                        current_data = new WeatherData(location_name, weather_type, Double.valueOf(current_temperature), Double.valueOf(min_temperature), Double.valueOf(max_temperature), timestamp);

                        // refresh top frame if response was received
                        refreshTopFrame(current_data);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Request responded with error!", Toast.LENGTH_SHORT).show();
            }
        });


        queue.add(request);
    }

    void refreshTopFrame(WeatherData data) {
        // get widgets
        TextView current_date_text = findViewById(R.id.current_date_text);
        TextView location_name_text = findViewById(R.id.location_name_text);
        TextView current_temperature_text = findViewById(R.id.current_temperature_text);
        TextView weather_type_text = findViewById(R.id.weather_type_text);
        ImageView weather_type_image = findViewById(R.id.weather_type_image_recycler);

        // set date format
        SimpleDateFormat date_format = new SimpleDateFormat("MMMM dd, yyyy ", Locale.ENGLISH);

        // set unit
        Switch fahrenheit_switch = findViewById(R.id.fahrenheit_switch);
        TextView temp_unit = findViewById(R.id.temp_unit);
        if (fahrenheit_switch.isChecked()) {
            temp_unit.setText("°F");
        } else {
            temp_unit.setText("°C");
        }

        // set widgets
        current_date_text.setText(date_format.format(new Date(Long.valueOf(data.getTimestamp()) * 1000)));
        location_name_text.setText(data.getLocation());
        current_temperature_text.setText(String.valueOf(data.getCurrentTemperature()));
        weather_type_text.setText(String.valueOf(data.getWeatherType()));
        // weather_type_image.setImageResource(); //todo: set weather type image correctly

        // stop spinner
        spinner.setVisibility(View.GONE);
    }

    void requestWeeklyForecast(double latitude, double longitude, boolean fahrenheit) {

        // unit types:
        // fahrenheit: &units=imperial
        // celsius: &units=metric

        //example link using celsius as unit:
        // http://api.openweathermap.org/data/2.5/forecast?lat=47.1257777&lon=17.8372092&appid=8d5bc773ee5432df7c786caba661b714&units=metric

        String url = "http://api.openweathermap.org/data/2.5/forecast?" +
                "lat=" + String.valueOf(latitude) +
                "&lon=" + String.valueOf(longitude) +
                "&appid=8d5bc773ee5432df7c786caba661b714";

        // set unit to request
        if (fahrenheit) {
            url += "&units=imperial";
        } else {
            url += "&units=metric";
        }

        // request queue
        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        // clear weekly data before inserting new one
                        weekly_data = new ArrayList<>();

                        // list holding dates
                        ArrayList<String> dates = new ArrayList<>();

                        String location_name = "LOCATION_NAME"; // -> string
                        String weather_type = "WEATHER_TYPE"; // -> string
                        String current_temperature = "0.0"; // -> double
                        String min_temperature = "0.0"; // -> double
                        String max_temperature = "0.0"; // -> double
                        String timestamp = "TIMESTAMP"; // -> string

                        try {
                            // find dates using regex
                            Matcher matcher;

                            // find dates
                            JSONArray list = response.getJSONArray("list");
                            for (int i = 0; i < list.length(); i++) {
                                JSONObject tmp_obj = list.getJSONObject(i);
                                String dt_txt = tmp_obj.getString("dt_txt");

                                matcher = Pattern.compile("\\w{4}-\\w{2}-\\w{2}", Pattern.MULTILINE).matcher(dt_txt);
                                if (matcher.find()) {
                                    String date = matcher.group(0);
                                    if (!dates.contains(date)) {
                                        dates.add(date);
                                    }
                                }
                            }

                            // find data for a specific date
                            for (int i = 0; i < dates.size(); i++) {
                                ArrayList<WeatherData> daily_data = new ArrayList<>();
                                for (int j = 0; j < list.length(); j++) {

                                    JSONObject list_obj = list.getJSONObject(j);
                                    String dt_txt = list_obj.getString("dt_txt");

                                    if (Pattern.compile(dates.get(i), Pattern.MULTILINE).matcher(dt_txt).find()) {

                                        try {
                                            // get location name
                                            JSONObject city_obj = response.getJSONObject("city");
                                            location_name = city_obj.getString("name");
                                        } catch (JSONException e) {
                                            Toast.makeText(MainActivity.this, "Requested data could not be parsed!", Toast.LENGTH_SHORT).show();
                                            Log.d("DEBUG", "Current location could not be determined!");
                                        }

                                        try {
                                            // get weather type
                                            JSONArray weather_array = list_obj.getJSONArray("weather");
                                            for (int k = 0; k < weather_array.length(); k++) {
                                                JSONObject weather_obj = weather_array.getJSONObject(k);
                                                weather_type = weather_obj.getString("main");
                                            }
                                        } catch (JSONException e) {
                                            Toast.makeText(MainActivity.this, "Requested data could not be parsed!", Toast.LENGTH_SHORT).show();
                                            Log.d("DEBUG", "Weather type could not be determined!");
                                        }

                                        try {
                                            // get temperatures
                                            JSONObject main_obj = list_obj.getJSONObject("main");

                                            current_temperature = main_obj.getString("temp");
                                            min_temperature = main_obj.getString("temp_min");
                                            max_temperature = main_obj.getString("temp_max");

                                        } catch (JSONException e) {
                                            Toast.makeText(MainActivity.this, "Requested data could not be parsed!", Toast.LENGTH_SHORT).show();
                                            Log.d("DEBUG", "Temperatures could not be determined!");
                                        }

                                        try {
                                            // get current date
                                            timestamp = list_obj.getString("dt");
                                        } catch (JSONException e) {
                                            Toast.makeText(MainActivity.this, "Requested data could not be parsed!", Toast.LENGTH_SHORT).show();
                                            Log.d("DEBUG", "Current date could not be determined!");
                                        }

                                        daily_data.add(new WeatherData(location_name, weather_type, Double.valueOf(current_temperature), Double.valueOf(min_temperature), Double.valueOf(max_temperature), timestamp));
                                    }
                                }
                                weekly_data.add(daily_data);
                            }

                            // refresh recyclerview if response was received
                            refreshRecyclerView(weekly_data);

                        } catch (JSONException e) {
                            Toast.makeText(MainActivity.this, "Requested data could not be parsed!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Request responded with error!", Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(request);
    }

    void refreshRecyclerView(ArrayList<ArrayList<WeatherData>> weekly_data) {
        RecyclerView weekly_forecast_recycler;

        // instantiate the recyclerview
        weekly_forecast_recycler = (RecyclerView) findViewById(R.id.weeklyforecastrecycler);
        weekly_forecast_recycler.setHasFixedSize(true); // improves performance

        // initialize the recyclerview adapter
        weekly_forecast_recycler.setAdapter(new WeeklyForecastRecyclerViewAdapter(this, weekly_data));

        // create a linear layout manager for the recyclerview
        weekly_forecast_recycler.setLayoutManager(new LinearLayoutManager(this));

        // stop spinner
        spinner.setVisibility(View.GONE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case FINE_LOCATION_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    registerLocationListener();
                    requestWeatherData();
                }
            }
            case COARSE_LOCATION_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    registerLocationListener();
                    requestWeatherData();
                }

            }
        }
    }

}
