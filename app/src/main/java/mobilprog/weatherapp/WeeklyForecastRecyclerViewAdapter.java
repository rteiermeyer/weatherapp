package mobilprog.weatherapp;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class WeeklyForecastRecyclerViewAdapter extends RecyclerView.Adapter<WeeklyForecastRecyclerViewAdapter.MyViewHolder> {

    class MyViewHolder extends RecyclerView.ViewHolder {
        // elements of listview_layout.xml
        TextView day_text;
        TextView weather_type_text;
        TextView max_temperature_text;
        TextView min_temperature_text;
        ImageView weather_type_image;
        ConstraintLayout weekly_forecast_recycler;

        // viewholder constructor
        MyViewHolder(View view) {
            super(view);
            day_text = view.findViewById(R.id.day_text_recycler);
            weather_type_text = view.findViewById(R.id.weather_type_text_recycler);
            max_temperature_text = view.findViewById(R.id.max_temperature_text_recycler);
            min_temperature_text = view.findViewById(R.id.min_temperature_text_recycler);
            weather_type_image = view.findViewById(R.id.weather_type_image_recycler);
            weekly_forecast_recycler = view.findViewById(R.id.weeklyforecastitemconstraint);
        }
    }

    final private ArrayList<ArrayList<WeatherData>> weekly_data;
    private Context context;

    // adapter constructor
    WeeklyForecastRecyclerViewAdapter(Context context, ArrayList<ArrayList<WeatherData>> weekly_data) {
        this.weekly_data = weekly_data;
        this.context = context;
    }

    // view creator function
    @Override
    public WeeklyForecastRecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder((View) LayoutInflater.from(parent.getContext()).inflate(R.layout.weeklyforecastrecyclerview_item, parent, false));
    }

    // initialize view content
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        String weather_type = "WEATHER_TYPE";
        double min_temperature = 999.999;
        double max_temperature = -999.999;
        String timestamp = "0.0";

        // data for a specific day -> current position
        final ArrayList<WeatherData> daily_data = weekly_data.get(position);

        // get lowest temperature for a specific day
        for(int i = 0; i < daily_data.size(); i++){
            if(daily_data.get(i).getMinTemperature() < min_temperature){
                min_temperature = daily_data.get(i).getMinTemperature();
            }
        }

        // get highest temperature for a specific day
        for(int i = 0; i < daily_data.size(); i++){
            if(daily_data.get(i).getMaxTemperature() > max_temperature){
                max_temperature = daily_data.get(i).getMaxTemperature();
            }
        }

        // get weather type frequency for a specific day
        Map<String, Integer> weather_type_frequency = new HashMap<>();
        for(int i = 0; i < daily_data.size(); i++){
            String tmp_weather_type = daily_data.get(i).getWeatherType();
            if(weather_type_frequency.containsKey(tmp_weather_type)){
                int value = weather_type_frequency.get(tmp_weather_type);
                weather_type_frequency.put(tmp_weather_type, value + 1);
            }
            else {
                weather_type_frequency.put(daily_data.get(i).getWeatherType(), 0);
            }
        }

        // get most frequent weather type
        weather_type = getMostFrequentWeatherType(weather_type_frequency);

        // get timestamp -> first timestamp of daily data
        timestamp = (weekly_data.get(position)).get(0).getTimestamp();

        // set date format
        SimpleDateFormat date_format = new SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH);

        holder.day_text.setText(date_format.format(new Date(Long.valueOf(timestamp) * 1000)));
        holder.weather_type_text.setText(weather_type);
        //holder.weather_type_image.setImageResource(); // todo: set image
        holder.min_temperature_text.setText(String.valueOf(min_temperature));
        holder.max_temperature_text.setText(String.valueOf(max_temperature));

        // bind onclick event -> todo: show detailed data
        holder.weekly_forecast_recycler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent details_activity = new Intent(context, ShowDetails.class);
                details_activity.putExtra("daily_data", daily_data);
                context.startActivity(details_activity);
            }
        });
    }

    @Override
    public int getItemCount() {
        return weekly_data.size();
    }

    private <K, V extends Comparable<V>> K getMostFrequentWeatherType(Map<K, V> map) {
        Map.Entry<K, V> maxEntry = Collections.max(map.entrySet(), new Comparator<Map.Entry<K, V>>() {
            public int compare(Map.Entry<K, V> e1, Map.Entry<K, V> e2) {
                return e1.getValue().compareTo(e2.getValue());
            }
        });
        return maxEntry.getKey();
    }
}
