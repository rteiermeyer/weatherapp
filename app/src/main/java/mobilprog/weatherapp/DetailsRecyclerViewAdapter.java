package mobilprog.weatherapp;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class DetailsRecyclerViewAdapter extends RecyclerView.Adapter<DetailsRecyclerViewAdapter.MyViewHolder> {
    class MyViewHolder extends RecyclerView.ViewHolder {
        // elements of listview_layout.xml
        TextView clock_text;
        TextView weather_type_text;
        TextView current_temperature_text;
        ImageView weather_type_image;
        ConstraintLayout details_recycler;

        // viewholder constructor
        MyViewHolder(View view) {
            super(view);
            clock_text = view.findViewById(R.id.details_clock);
            weather_type_text = view.findViewById(R.id.details_weather_type);
            current_temperature_text = view.findViewById(R.id.details_current_temperature);
            weather_type_image = view.findViewById(R.id.details_weather_type_image);
            details_recycler = view.findViewById(R.id.detailsrecycler);
        }
    }

    final private ArrayList<WeatherData> daily_data;
    private Context context;

    // adapter constructor
    DetailsRecyclerViewAdapter(Context context, ArrayList<WeatherData> daily_data) {
        this.daily_data = daily_data;
        this.context = context;
    }

    // view creator function
    @Override
    public DetailsRecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder((View) LayoutInflater.from(parent.getContext()).inflate(R.layout.detailsrecyclerview_item, parent, false));
    }

    // initialize view content
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        SimpleDateFormat date_format = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);

        holder.clock_text.setText(date_format.format(new Date(Long.valueOf(daily_data.get(position).getTimestamp()) * 1000)));
        holder.weather_type_text.setText(daily_data.get(position).getWeatherType());
        holder.current_temperature_text.setText(String.valueOf(daily_data.get(position).getCurrentTemperature()));

        // todo

    }

    @Override
    public int getItemCount() {
        return daily_data.size();
    }
}
